// Definicje DS2482-100
#define	DS_ADDR_0	0x30						// A0, A1 = 0
#define	DS_ADDR_1	0x36						// A0, A1 = 1
#define WCFG		0xD2						// write configuration
#define	DRST		0xF0						// device reset ( DS2482 )
#define	WRST		0xB4						// 1-wire reset
#define	WRBYTE		0xA5
#define	RDBYTE		0x96
#define	SETRP		0xE1						// set read pointer
// Definicje DS18B20
#define	SKPROM		0xCC
#define	CONVT		0x44
#define	READSC		0xBE

/* procedura odczytu temperatury przez DS18B20 */
void	Read_T( UCHAR ds_num, UCHAR *faza_p,UCHAR *bajt_p, int *temp_p ) {
	UCHAR	faza_x;
	UCHAR	bajt_x;
	int		temp_x;
	// przepisanie parametrow do zmiennych lokalnych
	faza_x = *faza_p;
	bajt_x = *bajt_p;
	temp_x = *temp_p;
	if( (faza_x >= 5) && (faza_x <= 104 ) )
 	   	faza_x++;								// czas dla DS18B20 na wykonanie "convert T"
	else
	switch( faza_x ) {
		case 0:	{
			WR_I2Cx2( ds_num, DRST );			// DS2482 reset
			faza_x++;
			break;
			}
		case 1:	{
			WR_I2Cx3( ds_num, WCFG, 0xE1 );		// DS2482 write config
			faza_x++;
			break;
			}
		case 2:	{
			WR_I2Cx2( ds_num, WRST );			// 1-wire reset
			faza_x++;
			break;
			}
		case 3:	{
			WR_I2Cx3( ds_num, WRBYTE, SKPROM );	// DS2482 write "skip rom" to 1-wire
			faza_x++;
			break;
			}
		case 4:	{
			WR_I2Cx3( ds_num, WRBYTE, CONVT );	// DS2482 write "convert T" to 1-wire
			faza_x++;
			break;
			}
		case 105: {
			WR_I2Cx2( ds_num, WRST );			// 1-wire reset
			faza_x++;
			break;
			}
		case 106: {
			WR_I2Cx3( ds_num, WRBYTE, SKPROM );	// DS2482 write "skip rom" to 1-wire
			faza_x++;
			break;
			}
		case 107: {
			WR_I2Cx3( ds_num, WRBYTE, READSC );	// DS2482 write "read scrachpad" to 1-wire
			faza_x++;
			break;
			}
		case 108: {
			WR_I2Cx2( ds_num, RDBYTE );			// DS2482 write "read 1-wire byte"
			faza_x++;
			break;
			}
		case 109: {
			WR_I2Cx3( ds_num, SETRP, 0xE1 );	// DS2482 write "set pointer", DS2482 read data reg
			faza_x++;
			break;
			}
		case 110: {
			bajt_x = RD_I2C( ds_num );			// Temp LSB
			faza_x++;
			break;
			}
		case 111: {
			WR_I2Cx2( ds_num, RDBYTE );			// DS2482 write "read 1-wire byte"
			faza_x++;
			break;
			}
		case 112: {
			WR_I2Cx3( ds_num, SETRP, 0xE1 );	// DS2482 write "set pointer", DS2482 read data reg
			faza_x++;
			break;
			}
		case 113: {
			temp_x = RD_I2C( ds_num );			// Temp MSB
			// obliczenie wyniku w stopniach C
			temp_x = ( temp_x << 8 ) + bajt_x;
			temp_x = ( temp_x * 10 ) >> 4;		// przeskalowanie, bo 1 stopien C = 16 ( 0x10 )
			faza_x++;
			break;
			}
		case 114: {
			WR_I2Cx2( ds_num, WRST );			// 1-wire reset -> terminate 1-wire read
			faza_x = 0;
			break;
			}
		default:
			break;
		}
	// aktualizacja zmiennych globalnych przed wyjsciem z procedury
	*faza_p = faza_x;
	*bajt_p = bajt_x;
	*temp_p = temp_x;
	return;
	}

