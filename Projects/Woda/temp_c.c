
// Definicje DS2482-100
#define	DS_ADDR_0	0x30						// A0, A1 = 0
#define	DS_ADDR_1	0x36						// A0, A1 = 1
#define WCFG		0xD2						// write configuration
#define	DRST		0xF0						// device reset ( DS2482 )
#define	WRST		0xB4						// 1-wire reset
#define	WRBYTE		0xA5
#define	RDBYTE		0x96
#define	SETRP		0xE1						// set read pointer
// Definicje DS18B20
#define	SKPROM		0xCC
#define	CONVT		0x44
#define	READSC		0xBE

/* Ustawienie I2C, 400kb/s !!!!!!!!!!!!!!!!! */

/* procedura odczytu temperatury przez DS18B20 */
void	Read_T( UCHAR ds_num, UCHAR *faza_p,UCHAR *bajt_p, int *temp_p ) {
	UCHAR	faza_x;
	UCHAR	bajt_x;
	int		temp_x;
	// przepisanie parametrow do zmiennych lokalnych
	faza_x = *faza_p;
	bajt_x = *bajt_p;
	temp_x = *temp_p;
	if( (faza_x >= 5) && (faza_x <= 104 ) )
 	   	faza_x++;								// czas dla DS18B20 na wykonanie "convert T"
	else
	switch( faza_x ) {
		case 0:	{
			WR_I2Cx2( ds_num, DRST );			// DS2482 reset
			faza_x++;
			break;
			}
		case 1:	{
			WR_I2Cx3( ds_num, WCFG, 0xE1 );		// DS2482 write config
			faza_x++;
			break;
			}
		case 2:	{
			WR_I2Cx2( ds_num, WRST );			// 1-wire reset
			faza_x++;
			break;
			}
		case 3:	{
			WR_I2Cx3( ds_num, WRBYTE, SKPROM );	// DS2482 write "skip rom" to 1-wire
			faza_x++;
			break;
			}
		case 4:	{
			WR_I2Cx3( ds_num, WRBYTE, CONVT );	// DS2482 write "convert T" to 1-wire
			faza_x++;
			break;
			}
		case 105: {
			WR_I2Cx2( ds_num, WRST );			// 1-wire reset
			faza_x++;
			break;
			}
		case 106: {
			WR_I2Cx3( ds_num, WRBYTE, SKPROM );	// DS2482 write "skip rom" to 1-wire
			faza_x++;
			break;
			}
		case 107: {
			WR_I2Cx3( ds_num, WRBYTE, READSC );	// DS2482 write "read scrachpad" to 1-wire
			faza_x++;
			break;
			}
		case 108: {
			WR_I2Cx2( ds_num, RDBYTE );			// DS2482 write "read 1-wire byte"
			faza_x++;
			break;
			}
		case 109: {
			WR_I2Cx3( ds_num, SETRP, 0xE1 );	// DS2482 write "set pointer", DS2482 read data reg
			faza_x++;
			break;
			}
		case 110: {
			bajt_x = RD_I2C( ds_num );			// Temp LSB
			faza_x++;
			break;
			}
		case 111: {
			WR_I2Cx2( ds_num, RDBYTE );			// DS2482 write "read 1-wire byte"
			faza_x++;
			break;
			}
		case 112: {
			WR_I2Cx3( ds_num, SETRP, 0xE1 );	// DS2482 write "set pointer", DS2482 read data reg
			faza_x++;
			break;
			}
		case 113: {
			temp_x = RD_I2C( ds_num );			// Temp MSB
			// obliczenie wyniku w stopniach C
			temp_x = ( temp_x << 8 ) + bajt_x;
			temp_x = ( temp_x * 10 ) >> 4;		// przeskalowanie, bo 1 stopien C = 16 ( 0x10 )
			faza_x++;
			break;
			}
		case 114: {
			WR_I2Cx2( ds_num, WRST );			// 1-wire reset -> terminate 1-wire read
			faza_x = 0;
			break;
			}
		default:
			break;
		}
	// aktualizacja zmiennych globalnych przed wyjsciem z procedury
	*faza_p = faza_x;
	*bajt_p = bajt_x;
	*temp_p = temp_x;
	return;
	}


/* procedura zapisu bajtu 'send_byte' przez I2C, oczekuje na ACK */	
void	I2C_WR( UCHAR send_byte ) {
	UCHAR	tmp;

	I2CDATA	= send_byte;
	tmp = I2CSTAT & 0x80;
	while ( !tmp )
		tmp = I2CSTAT & 0x80;					// czekaj na zakonczenie transmisji
	for ( tmp = 0; tmp <= 10; tmp++ )
		PDOUT ^= 0x01;							// daj odbiorcy troche czasu
	tmp = I2CSTAT & 0x20;						// czekaj na ACK od odbiorcy
	if ( !tmp )
		tmp = I2CSTAT & 0x20;
	return;
	}

/* procedura odbioru bajtu przez I2C oraz 'bit_ack' ( ACK/NAK ) */	
void	I2C_RD( UCHAR bit_ack ) {
	UCHAR	tmp;

	tmp = I2CSTAT & 0x40;
	while ( !tmp )
		tmp = I2CSTAT & 0x40;					// czekaj na bajt
	for ( tmp = 0; tmp <= 3; tmp++ )
		PDOUT ^= 0x01;							// daj sobie troche czasu
	tmp = I2CSTAT & bit_ack;					// czekaj na 'wlasne' ACK/NAK
	if ( !tmp )
		tmp = I2CSTAT & bit_ack;
	return;
	}

void	WR_I2Cx2( UCHAR adr, UCHAR cmd ) {
	I2CCTL	= I2C_IEN | I2C_START;				// I2C START
	I2C_WR( adr );								// adres, zapis 
	I2C_WR( cmd );
	I2CCTL	= I2C_IEN | I2C_STOP;				// I2C STOP
	for ( tmp_delay = 0; tmp_delay <= 10; tmp_delay++ )	// 10us
		PDOUT ^= 0x01;
	return;
	}

void	WR_I2Cx3( UCHAR adr, UCHAR cmd, UCHAR par ) {
	I2CCTL	= I2C_IEN | I2C_START;				// I2C START
	I2C_WR( adr );								// adres, zapis 
	I2C_WR( cmd );
	I2C_WR( par );
	I2CCTL	= I2C_IEN | I2C_STOP;				// I2C STOP
	for ( tmp_delay = 0; tmp_delay <= 10; tmp_delay++ )	// 10us
		PDOUT ^= 0x01;
	return;
	}

UCHAR	RD_I2C( UCHAR adr ) {
	UCHAR	tmp;

	I2CCTL	= I2C_IEN | I2C_START;				// I2C START
	I2C_WR( adr | 0x01 );						// adres, odczyt
	I2CCTL |= 0x04;								// wystaw NAK po odbiorze
	I2C_RD( I2C_NAK );
	tmp = I2CDATA;
	I2CCTL	= I2C_IEN | I2C_STOP;				// I2C STOP
	for ( tmp_delay = 0; tmp_delay <= 10; tmp_delay++ )	// 10us
		PDOUT ^= 0x01;
	return tmp;
	}

